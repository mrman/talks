# Talks #

This repository contains public talks that I've given, including the original content, slides, and generated output.

## Listing ##

- [What is a Backend? A selective guided tour through terminology, approaches, and technology](https://gitlab.com/mrman/talks/raw/master/dist/2019/04/mercari-backend-meetup.pdf) (2019-04-18 @ Mercari Engineers Backend Meetup)
- [Just Use Postgres](https://gitlab.com/mrman/talks/raw/master/dist/2019/06/just-use-postgres.condensed-more.pdf) (2019-06-04 @ Tokyo Tech Meetup)
- [GitLabで簡単コンテナ開発 (Japanese)](https://gitlab.com/mrman/talks/raw/master/dist/2019/06/gitlab-ci-and-docker-containers.pdf) (2019-06-04 @ Tokyo Tech Meetup)
