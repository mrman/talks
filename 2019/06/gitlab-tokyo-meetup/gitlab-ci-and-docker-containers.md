---
title: 「GitLab CI」で簡単コンテナ開発
author: Victor Adossi
theme: Madrid
fonttheme: professionalfonts
date: June, 2019
---

# トピック #

- コンテナとは？
- GitLab CIのコンテナフィーチャー
- コンテナビルド (GitLab + `dind`)
- コンテナ保存 (GitLab レジストリー)
- コンテナデプロイ (Gitlab CI)
- `docker`なしのコンテナ未来

# コンテナとは？ #

Linuxのコンテナは**プロセスを封じ込める**ツールです。

バーチャルマシン("VM")と違って、まったく新しいコンピューター作らず、プロセスの環境のコントロールで封じ込むんです。

&nbsp;

本当は色々のLinuxカーネルのフィーチャーを使ってコンテナと言うことが使えるようになりました。一番大事の二つは:

- ネームスペース(プロセスが見えるリソース)
- シーグループ(プロセスが使えるリソース)

&nbsp;

**一つのプロセスが見える世界(ファイルシステム、他のプロセス、デバイス)をコントロール出来るから、封じ込むことが出来ます。**

# GitLab CIのコンテナフィーチャー #

GitLab CIを使ってるなら、無料で色々のコンテナに関してのフィーチャーが使えます:

- コンテナ保存(レジストリー)
- コンテナビルド出来るCI
- セキュリティスキャン
- Kubernetesやコンテナ専門のインフラシステムまでデプロイ
- ...

# コンテナビルド #

簡単にCIのパイプラインの中でコンテナがビルド出来ます。

\tiny

```yaml
build_container:
  stage: build
  image: docker # ドッカーのバイナリが持っているイメージ
  services:
    - docker:dind # "Docker in docker" (ドッカーの中にドッカー)
  only: # リリースタッグとかリリース前のブランチだけに実行
    - /v[0-9|\.]+/ # release tags
    - /pre-release-v[0-9|\.]+/ # pre-release branches
  except: # 他のブランチで実行しない
    - branches
  script:
    - apk add --update ca-certificates make nodejs nodejs-npm
    - npm install
    - make docker-image
```

\normalsize

メークファイルの中には:

\tiny
```yaml
check-tool-docker:
ifndef DOCKER
    $(error "`docker` バイナリーがありません,`docker`をインストールしてください。(https://docs.docker.com)")
endif

docker-image: check-tool-docker
    docker build -f infra/docker/Dockerfile -t registry.gitlab.com/user/project:1.0.0 .
```

\normalsize
`dind`サービスと言うGitLabのフィーチャーを使って、CIのステップの中で`docker`が実行出来ます。

# コンテナ保存 #

無料で使えられるコンテナレジストリー

![](gitlab-container-registry-snapshot.png)

# コンテナ保存 (続) #

ビルド後レジストリーまでパブリッシュも簡単です:

\tiny

```yaml
publish:
  stage: publish
  image: docker
  services:
    - docker:dind
  only:
    - /v[0-9|\.]+/ # release tags
    - /pre-release-v[0-9|\.]+/ # pre-release branches
  except:
    - branches
  script:
    - apk add --update ca-certificates make nodejs nodejs-npm
    - npm install
    - docker login -u gitlab-ci-token --password $CI_BUILD_TOKEN registry.gitlab.com
    - make docker-image publish-docker-image
```

\normalsize

`CI_BUILD_TOKEN`はGitLab CIが毎回作ってくれるレジストリーまでプッシュのために使えるトークン。

メークファイルの中のターゲット:

\tiny

```bash
docker-image: ... # 変わりなし

publish-docker-image: check-tool-docker
    docker push registry.gitlab.com/user/project:1.0.0
```

# コンテナデプロイ #

色々のコンテナのデプロイ方があります:

- ベアメタルサーバー/インスタンス
- クラウド (AWS ElasticBeanstalk, Elastic Container Service, Google Cloud Platform, Azure Container Service)
- Kubernetes, Mesos, Docker Swarm

&nbsp;

どっちにしても効率がいいデプロイプロセス欲しいなら:

- どこかでコンテナをビルド
- どこかのレジストリーにコンテナをプッシュ
- どこかのCIを使って選んだプラットフォームを実行させる

&nbsp;

その三つの「どこか」が全部「GitLab」に出来ます!

# コンテナデプロイ (続) #

ビルドとパブリッシュする後デプロイはチーム次第:

\small

```yaml
deploy_staging:
  stage: deploy
  image: alpine
  only:
    - web # ウェブだけから実行出来る
  script:
    - apk add --update ca-certificates make openssh ...
    - make deploy
    # - ssh ....
    # - kubectl apply ....
    # - awscli ....
    - make deploy-check
    - make deploy-alert
```

# BONUS: CIのステップの中で使える「サービス」とは? #

Dockerのコンテナ何でも使えますシステムです。

\tiny

```yaml
e2e:
  stage: test
  only:
    - merge_requests # マージリクエストだけ実行する
  services:
    - name: postgres:10.4-alpine # データベース
    - name: jeanberu/mailcatcher:latest # メール
      alias: mailcatcher
    - name: minio/minio:latest # S3見たいのファイルストア
      alias: minio
      command: ["server", "/data"] # 特別のコマンド
  variables:
    # Postgresの設定
    POSTGRES_DB: db_name
    POSTGRES_USER: db_user
    POSTGRES_PASSWORD: db_password
    DB_HOST: postgres
    # Minioの設定
    MINIO_ACCESS_KEY: badkey
    MINIO_SECRET_KEY: badsecret
    # UGC
    S3_HOST: minio
    # Mailerの設定　
    MAILER_SMTP_HOST: mailcatcher
    MAILER_SMTP_PORT: 1025
  script:
    - apk add --update make
    - make setup build test-e2e # 実行する時に全部の上に書いてあるサービスを走ってる間E2Eテストを実行
```

# 終わり #

今日の皆さん時間頂いてありがとうございます。

# whoami

![](gaisma-logo.png){height=20%}

**アドッシー　ビクトル** (vados@gaisma.co.jp, vados@vadosware.io)

GPG: ED874DE957CFB552


日本で「ガイスマ」と言うコンサルティング会社をやっています、

&nbsp;

プレゼンについて補正とか質問がある方ぜひメールでメッセージしてください。

GitLabとか他の件についてのご相談がした方はぜひメールしてください。

&nbsp;

\small

こう言う技術的のコンテントが好きなら、個人的のブログもやっています (https://vadosware.io)
