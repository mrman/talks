.PHONY: all \
				2019-04-mercari-dev-meetup 2019-04-mercari-dev-meetup-pdf 2019-04-mercari-dev-meetup-condensed-pdf \
				2019-06-tokyo-tech-meetup 2019-06-tokyo-tech-meetup-pdf 2019-06-tokyo-tech-meetup-condensed-pdf 2019-06-tokyo-tech-meetup-condensed-more-pdf 2019-06-tokyo-tech-meetup-watch \
				2019-06-gitlab-tokyo-meetup 2019-06-gitlab-tokyo-meetup-pdf 2019-06-gitlab-tokyo-meetup-watch

all: 2019-04-mercari-dev-meetup 2019-06-tokyo-tech-meetup 2019-06-gitlab-tokyo-meetup

HTML_FORMAT ?= slidy
PDF_FORMAT ?= beamer
RESOURCE_PATH ?= .:resources
DATA_DIR ?= $(realpath data)
ENTR ?= entr

2019-04-mercari-dev-meetup-watch:
	find 2019/04/* | $(ENTR) -rc make 2019-04-mercari-dev-meetup

2019-04-mercari-dev-meetup: 2019-04-mercari-dev-meetup-pdf 2019-04-mercari-dev-meetup-condensed-pdf

2019-04-mercari-dev-meetup-pdf:
	pandoc \
	-t $(PDF_FORMAT) \
	--resource-path $(RESOURCE_PATH) \
	--data-dir $(DATA_DIR) \
	--self-contained \
	-s 2019/04/mercari-backend-meetup.md \
	-o dist/2019/04/mercari-backend-meetup.pdf

2019-04-mercari-dev-meetup-condensed-pdf:
	pandoc \
	-t $(PDF_FORMAT) \
	--resource-path $(RESOURCE_PATH) \
	--data-dir $(DATA_DIR) \
	--self-contained \
	-s 2019/04/mercari-backend-meetup-condensed.md \
	-o dist/2019/04/mercari-backend-meetup-condensed.pdf

2019-06-tokyo-tech-meetup: 2019-06-tokyo-tech-meetup-pdf 2019-06-tokyo-tech-meetup-condensed-pdf 2019-06-tokyo-tech-meetup-condensed-more-pdf

2019-06-tokyo-tech-meetup-watch:
	find 2019/06/* | $(ENTR) -rc make 2019-06-tokyo-tech-meetup

2019-06-tokyo-tech-meetup-pdf:
	@echo "DATA_DIR = $(DATA_DIR)"
	@mkdir -p dist/2019/06/tokyo-tech-meetup
	pandoc \
	-t $(PDF_FORMAT)+footnotes \
	--resource-path $(RESOURCE_PATH) \
	--data-dir $(DATA_DIR) \
	--self-contained \
	-s 2019/06/tokyo-tech-meetup/just-use-postgres.md \
	-o dist/2019/06/tokyo-tech-meetup/just-use-postgres.pdf

2019-06-tokyo-tech-meetup-condensed-pdf:
	@echo "DATA_DIR = $(DATA_DIR)"
	@mkdir -p dist/2019/06/tokyo-tech-meetup
	pandoc \
	-t $(PDF_FORMAT)+footnotes \
	--resource-path $(RESOURCE_PATH) \
	--data-dir $(DATA_DIR) \
	--self-contained \
	-s 2019/06/tokyo-tech-meetup/just-use-postgres.condensed.md \
	-o dist/2019/06/tokyo-tech-meetup/just-use-postgres.condensed.pdf

2019-06-tokyo-tech-meetup-condensed-more-pdf:
	@echo "DATA_DIR = $(DATA_DIR)"
	@mkdir -p dist/2019/06/tokyo-tech-meetup
	pandoc \
	-t $(PDF_FORMAT)+footnotes \
	--resource-path $(RESOURCE_PATH) \
	--data-dir $(DATA_DIR) \
	--self-contained \
	-s 2019/06/tokyo-tech-meetup/just-use-postgres.condensed-more.md \
	-o dist/2019/06/tokyo-tech-meetup/just-use-postgres.condensed-more.pdf

2019-06-gitlab-tokyo-meetup: 2019-06-gitlab-tokyo-meetup-pdf

2019-06-gitlab-tokyo-meetup-watch:
	find 2019/06/* | $(ENTR) -rc make 2019-06-gitlab-tokyo-meetup

2019-06-gitlab-tokyo-meetup-pdf:
	@echo "DATA_DIR = $(DATA_DIR)"
	@mkdir -p dist/2019/06/gitlab-tokyo-meetup
	pandoc \
	-t $(PDF_FORMAT)+footnotes \
	--resource-path $(RESOURCE_PATH) \
	--data-dir $(DATA_DIR) \
	--self-contained \
	--pdf-engine=xelatex \
	-V CJKmainfont="SourceHanSansJP-Regular" \
	-s 2019/06/gitlab-tokyo-meetup/gitlab-ci-and-docker-containers.md \
	-o dist/2019/06/gitlab-tokyo-meetup/gitlab-ci-and-docker-containers.pdf
